# node-magic

[![Gitlab pipeline status (branch)](https://img.shields.io/gitlab/pipeline/kyleafmine/node-magic/master.svg?logo=gitlab&style=for-the-badge)](https://gitlab.com/kyleafmine/node-magic/pipelines)
[![npm (scoped)](https://img.shields.io/npm/v/@thatonegamer999/node-magic.svg?style=for-the-badge)](https://www.npmjs.com/package/@thatonegamer999/node-magic)


WARNING: THIS MODULE DOES NOT WORK ON WINDOWS FOR NOW.

---


**[Read the documentation!](https://node-magic.netlify.com)**


A simple but fully featured wrapper around ImageMagick for node.js


Example of manipulating a png image:

```js
const Magic = require('@thatonegamer999/node-magic')
const fs = require('fs')
var cat = fs.readFileSync('cat.png')

new Magic.Wand(cat)
    .option('swirl', '90')
    .option('modulate', '100,300,300')
    .option('quality', '1')
    .option('resize', '25%')
    .render().then(image => {
        fs.writeFileSync('weirdcat.png', image)
    })
```

Example of a gif

```js
const Magic = require('@thatonegamer999/node-magic')
const fs = require('fs')
var cat = fs.readFileSync('cat.png')

new Magic.Broomstick()
    .newFrame(cat, 3)
    .frame(0).option('swirl', '33').done()
    .frame(1)
        .option('swirl', '66')
        .option('modulate', '100,150,100')
        .done()
    .frame(2).option('swirl', '46').done()
    .render().then(gif => {
        fs.writeFileSync('catswirl.gif', gif)
    })
```

Example of creating a `Broomstick` from a gif

```js
const Magic = require('@thatonegamer999/node-magic')
const fs = require('fs')
var cat = fs.readFileSync('catswirl.gif')

Magic.Broomstick.from(cat).then(stick => {
    stick
        .newFrame(cat, 3)
        .frame(0).option('swirl', '33').done()
        .frame(1).option('swirl', '66').done()
        .frame(2).option('swirl', '46').done()
        .option('modulate', '100,500,100')
        .render().then(gif => {
            fs.writeFileSync('catswirlbutmoreswirledandsaturated.gif', gif)
})
```