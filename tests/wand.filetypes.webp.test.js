/*eslint-disable no-undef, semi, unknown-require*/
const Magic = require('../src/index')
const fs = require('fs')
var cat = fs.readFileSync('./tests/cat.png')
const crypto = require('crypto')
const child_process = require('child_process')


var b = new Magic.Wand(cat)

test('webp', (done) => {
    var b = new Magic.Wand(cat)
    b.option('swirl', '33')
    b.render("webp").then(b => {
        expect(b).toBeTruthy()
        expect(b.length).toBeGreaterThan(128)
        done()
    }).catch(e => {
        console.log('webp test failed, but allowed to fail.')
        done()
    })
})