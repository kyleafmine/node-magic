/*eslint-disable no-undef, semi, unknown-require*/
const Magic = require('../src/index')
const fs = require('fs')
var cat = fs.readFileSync('./tests/cat.png')
const crypto = require('crypto')
const child_process = require('child_process')
var stick = new Magic.Broomstick()
var cat = fs.readFileSync('./tests/cat.png')

describe('loading images', () => {
    test('blank gif', () => {
        var x = new Magic.Broomstick()
        expect(x.frames).toEqual([])
    })
    test('from file', (done) => {
        //console.log('start file read')
        var gif = fs.readFileSync('./tests/cat_expect.gif')
        //console.log('file read')
        Magic.Broomstick.from(gif).then(g => {
            expect(g.frames.length).toEqual(4)
            done()
        })
    })
    
})

describe('maniuplating gifs', () => {
    test('adding frames', () => {
        stick.newFrame(cat, 4)
        expect(stick.frames.length).toEqual(4)
    })
    test('selecting frames', () => {
        expect(stick.frame(0)).toEqual(stick.frames[0])
        expect(stick.frame(0)).toBeInstanceOf(Magic.Frame)
    })
})

describe('rendering gifs', () => {
    test('render', (done) => {
        jest.setTimeout(15000)
        stick.render().then(b => {
            expect(b.length).toBeGreaterThan(128)
            done()
        })
    }) 
}) 