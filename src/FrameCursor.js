



/**
 * Utility class for scrolling through frames
 * 
 */

 class FrameCursor {
    /**
     * @param {Array} frames - Array of frames
     * @param {Broomstick} self - Parent Broomstick instance of this cursor
     * @returns {FrameCursor}
     */
        constructor(frames, self){
            /**
             * Parent {Broomstick} instance
             */
            this.self = self
            /**
             * Array of frames
             */
            this.frames = frames
            /**
             * Current frame
             */
            this.frame = frames[0]
            /**
             * Index of current frame
             */
            this.index = 0

        }
        /**
         * Moves cursor to next frame in gif. Returns the parent Broomstick instance if there is none.
         * @returns {Frame}
         */
        next() {
            this.index++
            this.frame = this.frames[this.index]
            if (!this.frame) {
                return this.self
            }
            return this
        }
        /**
         * Returnes previous frame in gif. If there is none, returns parent Broomstick instance.
         * @returns {Frame}
         */
       prev() {
            this.index--
            this.frame = this.frames[this.index]
            if (!this.frame) {
                return this.self
            }
            return this
        }
        
        option(opt, val) {
            this.frame.option(opt, val)
            return this
        }
        /**
         * Returns parent Broomstick instance
         * @returns {Broomstick}
         */
        done() {
            return this.self
        }
        /**
         * Runs a function for each frame in the gif
         * @callback fun - Will be passed a {Frame} instance
         */
        each(fun) {
            for (var frame of this.frames) {
                fun(frame)
            }
            return this
        } 
}

module.exports = FrameCursor