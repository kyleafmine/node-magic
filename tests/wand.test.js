/*eslint-disable no-undef, semi, unknown-require*/
const Magic = require('../src/index')
const fs = require('fs')
var cat = fs.readFileSync('./tests/cat.png')
const crypto = require('crypto')
const child_process = require('child_process')


var b = new Magic.Wand(cat)
describe('reading images', () => {
    test('stores image buffer', () => {
        var b = new Magic.Wand(cat)
        expect(b.buffer.toString('base64')).toEqual(cat.toString('base64'))
    })
})
describe('manipulating images', () => {
    test('setting options', () => {
        b.option('swirl', '90')
        expect(b.output.options.swirl).toEqual('90')
    })
    test('magik', () => {
        b.magik()
        expect(b.output.options['liquid-rescale']).toEqual('700x467')
        expect(b.input.options['liquid-rescale']).toEqual('40%')
    })
})
describe('rendering', () => {
    it('renders an image to png', (done) => {
        b.render().then(buf => {
            var process = child_process.exec('compare -metric phash png:- ./tests/cat_expect.png null:', {}, function(e, out, ee){
                expect(parseFloat(ee)).toBeLessThan(2)
                done()
            })
            
            process.stdin.end(buf)

        })
    })
    
})
