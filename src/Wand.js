/*eslint-disable no-unreachable */
var child_process = require('child_process');
const uuid = require('uuid/v4');
const sizeOf = require('image-size')
/**
 * Bass class for static image editing
 */
class Wand {
    /**
     * Base class for static image editing
     * @access public
     * @param {Buffer} buffer - A buffer representing any format of image compatible with ImageMagick
     * @returns {Wand} - A Magic Wand of that image 
     */
    constructor(buffer) {
        /**
         * Buffer of image for this Wand
         * @public
         * @type {Buffer}
         */
        this.buffer = buffer;
        this.output = {
            options: {},
            string: () => {
                var a = [];
                for (var opt in this.output.options) {
                    a.push(`-${opt} ${this.output.options[opt]}`);
                }
                return a.join(' ').replace("undefined", "");
            }
        };
        this.input = {
            options: {},
            string: () => {
                var a = [];
                for (var opt in this.input.options) {
                    a.push(`-${opt} ${this.input.options[opt]}`);
                }
                return "\\( - " + a.join(' ') + " \\)";
            }
        };
    }
    /**
     * Set ImageMagick option for this image
     * @param {string} opt - Option key
     * @param {string} val - Option value
     * @returns {Wand} - This Wand object so you can chain functions
     * @example
     * new Magic.Broomstick(cat)
     *  .option('emboss', '30')
     *  .render().then(x => fs.writeFileSync('embossed_cat.png', x))
     */
    option(opt, val) {
        this.output.options[opt] = val;
        return this;
    }
    /**
     * Option for the input image. Useful if you want to distort an image by swirling then
     * unswirling it
     * @param {string} opt - Option key
     * @param {string} val - Option value
     * @returns {Wand} - This Wand object so you can chain functions
     */
    inputOption(opt, val) {
        this.input.options[opt] = val;
        return this;
    }
    /**
     * Magiks the image, resulting in content-aware distortion
     * @param {number} [intensity=6] - How intense should the image be distorted
     * @returns {Wand} - this Wand object so you can chain functions
     */
    magik(intensity=6) {
        var i = (100-(intensity * 10)).toString();
        this.inputOption('liquid-rescale', `${i}%`);
        var size = sizeOf(this.buffer);
        this.output.options['liquid-rescale'] = `${size.width}x${size.height}`;
        
        return this;
    }
    /**
     * Swirl, will be deprecated
     * @deprecated Use {@link Wand#option} instead
     * @param {number} [degrees=90] - Degrees of swirling. Can be negative
     * @returns {Wand} - This wand object so you can chain functions
     */
    swirl(degrees=90) {
        
        this.option('swirl', degrees.toString());
        
        return this;
    } 
    /**
     * Renders an image to a Buffer of specified type
     * @param {string} [format=png] - Output format
     * @returns {Promise<Buffer>|Promise<String>}
     */
    render(format="png") {
        return new Promise((resolve, reject) => {
            var optstring = ""
            for (var opt in this.output.options) {
                optstring += `-${opt} ${this.output.options[opt]} `
            }
            var nf = format.replace('valuethatwillneverbeinthisstring', "")
            format = format.replace(':base64', '')
            
            
           
            var program = child_process.exec(`convert ${this.input.string()} ${this.output.string()} ${format}:- | base64`, {maxBuffer: 1024 * 5000}, function(e, out, ee) {
                //console.log(e, ee)
                if (ee && !ee.includes('warning')) {
                    return reject(new Error(ee))
                }
                if (nf.includes('base64')) {
                    resolve(out)
                } else {
                resolve(Buffer.from(out, "base64"))
            }

            });
            program.stdin.end(this.buffer)
            
            
            
        })
    }


}
module.exports = Wand