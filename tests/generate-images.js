const Magic = require('../src/index')
const fs = require('fs')
var cat = fs.readFileSync('./tests/cat.png')

new Magic.Wand(cat)
    .option('swirl', '90')
    .magik()
    .render().then(m => {
        fs.writeFileSync('./output/cat_output.png', m)
    })
    
var stick = new Magic.Broomstick()
stick.newFrame(cat, 4)
stick
    .frame(1).option('swirl', '33').done()
    .frame(2).option('swirl', '66').done()
    .frame(3).option('swirl', '33').done()
    .option('resize', '75%')
    .render().then(m => {
        fs.writeFileSync('./output/cat_output.gif', m)
    })