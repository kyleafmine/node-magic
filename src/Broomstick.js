/*eslint-disable no-unreachable */
var child_process = require('child_process');
const uuid = require('uuid/v4');
const sizeOf = require('image-size')
const fs = require('fs')
const Frame = require('./Frame')
/**
 * Class used for dealing with gifs, or creating gifs out of static images
 * @param {string} [id=uuid] - Id of this broomstick. DO NOT SET UNLESS YOU ARE 100% SURE YOU KNOW WHAT YOU ARE DOING.
 * @returns {Broomstick}
 */

 class Broomstick {
    constructor(id) {
        if (!id) id = uuid()
        /** @type {String} */
        this.id = id
        /**
         * Array of frames
         * @type {Array<Frame>}
         */
        this.frames = [];
    }
    
    /**
     * Creates a Broomstick from a gif buffer
     * @param {Buffer} buffer - Buffer of a gif file
     * @returns {Promise<Broomstick>}
     */
    static from(buffer) {
        //console.log('start start')
        return new Promise((resolve, reject) => {
            //console.log('start')
            var id = "node-magic-" + uuid();
            var b = new Broomstick(id)
            fs.mkdirSync('/tmp/' + id);
            //console.log('start')
            var program = child_process.exec(`convert gif:- /tmp/${id}/split.png`, {}, function(e, out, ee) {
                //console.log('done')
                console.log(e, ee)
                if (ee) return reject(new Error(ee));
                var files = fs.readdirSync('/tmp/' + id);
                
                for (var file of files) {
                    var num = parseInt(file.match(/[0-9]/))
                    
                    b.frames.push(new Frame(fs.readFileSync(`/tmp/${id}/${file}`), id, num, this))
                    
                }
                //console.log('done')
                resolve(b)
                
            });
            program.stdin.end(buffer);
    });

    }
    /**
     * Selects frame by index. Returns undefined if does not exist
     * @param {number} num - Index of frame
     * @returns {Frame}
     */
    frame(num) {
        return this.frames[num]
    }
    /**
     * Creates a frame from a buffer and appends it to the gif. Useful for creating gifs out of still images
     * @param {Buffer} buffer - Image buffer. Can be any type supported by ImageMagick
     * @param {number} [count=1] - Amount of frames this image should be
     * @returns {Broomstick}
     */
    newFrame(buffer, count=1) {
        for (var i = 1; i <= count; i++) {
            this.frames.push(new Frame(buffer, this.id, this.frames.length, this))
        } 
        
        return this
    }
    /**
     * Sets an option for the whole gif. Useful for saturating an entire gif for example.
     * @param {string} opt - Option key
     * @param {string} val - Option value
     * @returns {Broomstick}
     */
    option(opt, val) {
        for (var f of this.frames) {
            f.option(opt, val)
        }
        return this
    }
    /**
     * Creates a new FrameCursor of this Broomstick instance
     * @returns {FrameCursor}
     */
    cursor() {
        return new FrameCursor(this.frames, this)
    }
    /**
     * Renders gif to a buffer.
     * @returns {Promise<Buffer>}
     */
    render() {
        return new Promise(async (resolve, reject) => {
            
            
            
            
            if (fs.existsSync(`/tmp/${this.id}`)) {
                child_process.execSync(`rm /tmp/${this.id}/**`)
            } else {
                fs.mkdirSync(`/tmp/${this.id}`)
            }

            var promises = []
            for (var frame of this.frames) {
                promises.push(frame.render())
            }
            await Promise.all(promises)
            child_process.exec(`convert /tmp/${this.id}/*.png gif:- | base64`, {maxBuffer: 1024 * 10000}, (e, out, ee) => {
                if (ee && !ee.includes('warning')) {
                    reject(new Error(ee))
                }
                
                
                resolve(Buffer.from(out, 'base64'))
                child_process.execSync('rm -r /tmp/' + this.id)
            })
           
           
            
        })
        
    }


}
module.exports = Broomstick