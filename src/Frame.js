const child_process = require('child_process')
const fs = require('fs')
const Wand = require('./Wand')

/**
 * Utility class used internally. Do *NOT* use in your code unless you really need to
 * @extends {Wand}
 */
 class Frame extends Wand {
    /**
     * @param {Buffer} buffer - Image buffer
     * @param {string} id - Id of folder in /tmp
     * @param {number} index - Frame index of this frame
     * @param {Broomstick} - Parent Broomstick of this frame. Used for stuff like .done()
     * @returns {Frame}
     */
    constructor(buffer, id, index, parent) {
        super();
        this.buffer = buffer
        this.index = index;
        this.id = id;
        this.parent = parent
    }
        render(format="png") {
       
        return new Promise((resolve, reject) => {
            var index = this.index.toString()
            if (index.length == 1) {
                index = "0" + index
            }
           var program = child_process.exec(`convert ${this.input.string()} ${this.output.string()} /tmp/${this.id}/${index}.png`, {maxBuffer: 1024 * 5000}, function(e, out, ee) {
            
                if (ee && !ee.includes('warning')) {
                    return reject(new Error(ee));
                }
                resolve(true);
            });
            program.stdin.end(this.buffer);
            
            
        });
    }
    /**
     * Returns parent boomstick. Used for chaining functions.
     * @returns {Broomstick}
     */
    done() {
        return this.parent
    }
}
module.exports = Frame